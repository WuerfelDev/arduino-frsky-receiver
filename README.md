# Arduino Frsky receiver

Connecting a Frsky D16 receiver to an Arduino via SBUS. Jumper R1, XR8, XR4

## Wiring

The Frsky SBUS protocol is inverted, therefore we need to un-invert it before it is read. Most receivers have a pin onboard which is already the uninverted signal. Check it here: https://oscarliang.com/uninverted-sbus-smart-port-frsky-receivers

<details>
<summary markdown="span">Use FTDI for inverting</summary>

---

_Not tried, does the FTDI only connect to a computer or can it be used for connecting to an Arduino?_

- https://www.youtube.com/watch?v=UAR65jER6WY
- https://www.youtube.com/watch?v=_-EouL2nNgE

---

</details>
<details>
<summary markdown="span">Create your own inverter</summary>

---

You can easily create your own inverter:
https://www.youtube.com/watch?v=DtvID1YeSbE

![https://web.archive.org/web/20181119200544/https://oscarliang.com/ctt/uploads/2015/12/sbus-inverter-diagram-schematics.jpg](https://oscarliang.com/ctt/uploads/2015/12/sbus-inverter-diagram-schematics.jpg)
![fritzing](img/inverter.png)

---

</details>


<details>
<summary markdown="span">Using Software...</summary>

---

_Not tried, you should prefer the other options to avoid SoftwareSerial_

I saw that SoftwareSerial has an option for inverted signal and thought that it might work. But it seems that it [doesn't support a 100000 baud rate](https://www.arduino.cc/en/Reference/SoftwareSerialBegin).

[Arduino SoftwareSerial](https://www.arduino.cc/en/Reference/softwareSerial)

---

</details>


## Example

- [Plotting the received channenls](plot/plot.ino)

## Resources

- https://github.com/robotmaker/Arduino_SBUS
